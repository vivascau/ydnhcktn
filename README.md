Yahoo Hackaton!
===============


Idea
-----

Create an app in which you display Facebook images of your friends and people need to recognise them.

app name:     > fabme

Users will open the app on their phone, they will connect to a room using facebook login. 
Once more than 2 users game will start in 15 seconds when room will be locked for access.
On a Desktop/SmartTV you will be able to access the room where these players are playing.

GamePlay?
---------
Users will be presented on devices with a list of  3 randomly selected friends name and profile image from their FB list. 
Game will merge all friends list on the card plus few more (5) of each player to a bigger list that game will hold. For example if there are 3 users playing there will be a total of (3+5)*3 = 24 FB images in the list.
Game will extract from the big list a random FB friend profile image and present it to all users.
Users will need to recognize if one of their friends have been extracted and they have it on the list.  If that happens they will need to dab it. 
Winner is the user who first dabs all his friends.

You will also display the images on a room server that is on a desktop (Chrome browser) . On the device you will have the users dabbing if they recognize their friends. On the desktop server you will have the game showing the cards of all users and what they have dabbed. 


Tools
-----

Webstorm
node.js (sockets.io)
Yahoo cocktails (YUI3, YQL, Mojito)
FB API